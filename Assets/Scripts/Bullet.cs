using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name != "Character" && other.name != "Bullet" && !other.name.Contains("Shield") && !other.name.Contains("Ammunition"))
        {
            gameObject.SetActive(false);
        }
    }
}
