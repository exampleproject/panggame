using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    public int MovementSpeed = 10;
    public Transform StartFirePosition;
    public GameObject Shield;
    public float ShieldTime = 5;
    public float AmmunitionTime = 2.5f;

    private int _ammunitionLevel = 1;
    private bool _isMove;
    private float _startXPos;

    private void Awake()
    {
        _startXPos = transform.position.x;
    }

    public void OnMoveClick(bool isRight)
    {
        if (!isRight)
        {
            MovementSpeed *= -1;
        }
        _isMove = true;
    }

    public void OnMoveStopped()
    {
        MovementSpeed = Mathf.Abs(MovementSpeed);
        _isMove = false;
    }

    public void OnFireClick()
    {
        for (var i = 0; i < _ammunitionLevel; i++)
        {
            var bullet = StackManager.Stack.GetObject(StackManager.StackList.Bullets, transform);
            bullet.name = "Bullet";
            bullet.transform.position = StartFirePosition.position;
            bullet.transform.rotation = Quaternion.identity;
            var angle = 10 * i / 2;
            if (i % 2 == 1)
            {
                angle *= -1;
            }
            bullet.transform.Rotate(0, 0, angle);
            bullet.GetComponent<Rigidbody2D>().velocity = bullet.transform.up * 500;
        }
    }

    public void SetShield()
    {
        Shield.SetActive(true);
        StartCoroutine(ShieldDown());
    }

    IEnumerator ShieldDown()
    {
        yield return new WaitForSeconds(ShieldTime);
        Shield.SetActive(false);
    }

    public void SetAmmunition(int ammunitionLevel)
    {
        _ammunitionLevel = ammunitionLevel;
        StartCoroutine(BackToAmmunitionLevelOne());
    }

    IEnumerator BackToAmmunitionLevelOne()
    {
        yield return new WaitForSeconds(AmmunitionTime);
        _ammunitionLevel = 1;
    }

    private void Update()
    {
        if (_isMove)
        {
            transform.position += Vector3.right * MovementSpeed * Time.deltaTime;
            if (transform.position.x > _startXPos + 900)
            {
                transform.position = new Vector3(_startXPos + 900, transform.position.y, transform.position.z);
            }
            if (transform.position.x < _startXPos - 900)
            {
                transform.position = new Vector3(_startXPos - 900, transform.position.y, transform.position.z);
            }
        }
    }
}
