using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ammunition : MonoBehaviour
{
    public float FallSpeed = 20;
    private RectTransform _rect;
    private int _ammunitionLevel;

    private void OnEnable()
    {
        _ammunitionLevel = Random.Range(0, 1) == 1 ? 3 : 5;
    }

    private void Start()
    {
        _rect = GetComponent<RectTransform>();
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name == "Character")
        {
            AppManager.App.Player.SetAmmunition(_ammunitionLevel);
            gameObject.SetActive(false);
        }
    }

    private void Update()
    {
        _rect.position += Vector3.down * FallSpeed * Time.deltaTime;
        if (_rect.anchoredPosition.y < -310)
        {
            _rect.anchoredPosition = new Vector2(_rect.anchoredPosition.x, -310);
        }
    }
}
