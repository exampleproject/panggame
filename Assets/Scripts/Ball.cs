using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public int BallSize = 1;
    public int MaxBallSize = 3;
    public int SplitTo = 2;
    public bool IsRight = true;
    public Vector2 BaseVelocity = new Vector2(100, 100);
    public Vector2 StartPosition { get; set; }

    private Rigidbody2D _rigidbody;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    private void OnEnable()
    {
        _rigidbody.velocity = new Vector2(0, 0);
    }

    public void SetBall()
    {
        transform.localScale = Vector3.one / BallSize;
        GetComponent<RectTransform>().anchoredPosition = StartPosition;
        _rigidbody.velocity = BaseVelocity * (BallSize + Random.Range(-5, 5));
        if (!IsRight)
        {
            _rigidbody.velocity = new Vector2(_rigidbody.velocity.x * -1, _rigidbody.velocity.y);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name == "Bullet")
        {
            var nextSize = BallSize + 1;
            gameObject.SetActive(false);
            if (BallSize < MaxBallSize)
            {
                other.isTrigger = false;
                for (var i = 0; i < SplitTo; i++)
                {
                    CreateBall(i % 2 == 1, nextSize);
                }
                StackManager.Stack.GetComponent<MonoBehaviour>().StartCoroutine(WaitForDistance(other));
            }
            else
            {
                if (StackManager.Stack.IsLastBall())
                {
                    AppManager.App.Success();
                    return;
                }
                switch(Random.Range(1, 5))
                {
                    case 2:
                        var shield = StackManager.Stack.GetObject(StackManager.StackList.Shields, GameObject.Find("Canvas").transform);
                        shield.transform.position = transform.position;
                        break;
                    case 4:
                        var ammunition = StackManager.Stack.GetObject(StackManager.StackList.Ammunition, GameObject.Find("Canvas").transform);
                        ammunition.transform.position = transform.position;
                        break;
                }
            }
        }
        else if(other.name == "Character")
        {
            StopAllCoroutines();
            AppManager.App.Faild();
        }
    }

    IEnumerator WaitForDistance(Collider2D bullet)
    {
        yield return new WaitForSeconds(0.5f);
        bullet.isTrigger = true;
    }

    private void CreateBall(bool isRight, int ballSize)
    {
        Ball ball = StackManager.Stack.GetObject(StackManager.StackList.Balls, StackManager.Stack.transform).GetComponent<Ball>();
        ball.BallSize = ballSize;
        ball.StartPosition = GetComponent<RectTransform>().anchoredPosition;
        ball.IsRight = isRight;
        ball.gameObject.SetActive(true);
        ball.SetBall();
    }
}
