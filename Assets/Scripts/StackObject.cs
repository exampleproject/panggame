﻿using System.Collections.Generic;
using UnityEngine;

public class StackObject : MonoBehaviour
{
    public StackManager.StackList Stack;
    private void OnDisable()
    {
        StackManager.Stack.AddToStack(Stack, gameObject);
    }
}