using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class StackManager : MonoBehaviour
{
    public enum StackList { Balls, Bullets, Shields, Ammunition }

    public string BallResourcePath = "Ball";
    public string BulletResourcePath = "Bullet";
    public string ShieldResourcePath = "Shield";
    public string AmmunitionResourcePath = "Ammunition";

    private List<GameObject> _allBalls = new List<GameObject>();
    private List<GameObject> _allBullets = new List<GameObject>();
    private List<GameObject> _allShields = new List<GameObject>();
    private List<GameObject> _allAmmunition = new List<GameObject>();
    private List<GameObject> _availableBalls = new List<GameObject>();
    private List<GameObject> _availableBullets = new List<GameObject>();
    private List<GameObject> _availableShields = new List<GameObject>();
    private List<GameObject> _availableAmmunition = new List<GameObject>();

    public static StackManager Stack;
    private void Awake()
    {
        if (Stack == null)
        {
            Stack = this;
            return;
        }
        Destroy(gameObject);
    }

    public GameObject GetObject(StackList stackList, Transform parent = null)
    {
        List<GameObject> stack;
        List<GameObject> fullStack;
        string path;
        switch (stackList)
        {
            case StackList.Balls:
                stack = _availableBalls;
                fullStack = _allBalls;
                path = BallResourcePath;
                break;
            case StackList.Bullets:
                stack = _availableBullets;
                fullStack = _allBullets;
                path = BulletResourcePath;
                break;
            case StackList.Shields:
                stack = _availableShields;
                fullStack = _allShields;
                path = ShieldResourcePath;
                break;
            case StackList.Ammunition:
                stack = _availableAmmunition;
                fullStack = _allAmmunition;
                path = AmmunitionResourcePath;
                break;
            default:
                return null;
        }

        GameObject returnObject;

        if (stack.Count > 0)
        {
            returnObject = stack[0];
            stack.RemoveAt(0);
            returnObject.SetActive(true);
        }
        else
        {
            returnObject = Instantiate(Resources.Load<GameObject>(path), parent);
            fullStack.Add(returnObject);
        }
        returnObject.AddComponent<StackObject>().Stack = stackList;
        return returnObject;
    }

    public void AddToStack(StackList stackList, GameObject objectToAdd)
    {
        switch (stackList)
        {
            case StackList.Balls:
                if (!_availableBalls.Contains(objectToAdd))
                {
                    _availableBalls.Add(objectToAdd);
                }
                break;
            case StackList.Bullets:
                if (!_availableBullets.Contains(objectToAdd))
                {
                    _availableBullets.Add(objectToAdd);
                }
                break;
        }
    }

    public void ResetBoard()
    {
        foreach(var ball in _allBalls)
        {
            Destroy(ball);
        }
        _allBalls.Clear();

        foreach(var bullet in _allBullets)
        {
            Destroy(bullet);
        }
        _allBullets.Clear();

        foreach(var shield in _allShields)
        {
            Destroy(shield);
        }
        _allShields.Clear();

        foreach(var ammunition in _allAmmunition)
        {
            Destroy(ammunition);
        }
        _allAmmunition.Clear();

        _availableBalls.Clear();
        _availableBullets.Clear();
        _availableShields.Clear();
        _availableAmmunition.Clear();
    }

    public bool IsReset()
    {
        return _allBalls.Count == 0 && _availableBalls.Count == 0 && _allBullets.Count == 0 && _availableBullets.Count == 0 && transform.childCount == 0;
    }

    public bool IsLastBall()
    {
        foreach(var ball in _allBalls)
        {
            if (ball.activeInHierarchy)
            {
                return false;
            }
        }
        return true;
    }
}
