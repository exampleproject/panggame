using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour
{
    public float FallSpeed = 20;
    private RectTransform _rect;

    private void Start()
    {
        _rect = GetComponent<RectTransform>();
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name == "Character")
        {
            AppManager.App.Player.SetShield();
            gameObject.SetActive(false);
        }
    }

    private void Update()
    {
        _rect.position += Vector3.down * FallSpeed * Time.deltaTime;
        if (_rect.anchoredPosition.y < -310)
        {
            _rect.anchoredPosition = new Vector2(_rect.anchoredPosition.x, -310);
        }
    }
}
