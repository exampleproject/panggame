using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppManager : MonoBehaviour
{
    public static AppManager App;
    public List<LevelParameters> Levels;
    public Character Player;
    public Menu MenuScript;

    public int CurrentLevel { get; set; }

    private void Awake()
    {
        if (App != null)
        {
            Destroy(gameObject);
            return;
        }
        App = this;
    }

    public void PlayNextLevel()
    {
        CurrentLevel++;
        StartLevel();
    }

    public void ResetLevel()
    {
        StackManager.Stack.ResetBoard();
        StartCoroutine(WaitForDestroy());
    }

    IEnumerator WaitForDestroy()
    {
        while(!StackManager.Stack.IsReset())
        {
            yield return null;
        }
        StartLevel();
    }

    private void StartLevel()
    {
        foreach(var ball in Levels[CurrentLevel].Balls)
        {
            var ballScript = StackManager.Stack.GetObject(StackManager.StackList.Balls, StackManager.Stack.transform).GetComponent<Ball>();
            ballScript.BallSize = ball.Size;
            ballScript.StartPosition = ball.Position;
            ballScript.SetBall();
        }
        MenuScript.SetMenuMode(Menu.MenuMode.OnPlay);
    }

    public void Faild()
    {
        StackManager.Stack.ResetBoard();
        MenuScript.OnMenu();
    }

    public void Success()
    {
        StackManager.Stack.ResetBoard();
        MenuScript.SetMenuMode(Menu.MenuMode.OnStop);
        MenuScript.OnMenu();
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
}

[Serializable]
public class LevelParameters
{
    [HideInInspector]
    public string name = "Level";
    public List<BallParameters> Balls;
}

[Serializable]
public class BallParameters
{
    [HideInInspector]
    public string name = "Ball";
    public int Size;
    public Vector2 Position;
}
