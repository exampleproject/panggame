using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    public enum MenuMode { OnPlay, OnStop }

    public Button NextLevelButton;
    public Button ReplayButton;

    private Animator _animator;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    public void OnMenu()
    {
        _animator.SetBool("IsOpen", !_animator.GetBool("IsOpen"));
    }

    public void SetMenuMode(MenuMode mode)
    {
        switch (mode)
        {
            case MenuMode.OnPlay:
                NextLevelButton.gameObject.SetActive(false);
                ReplayButton.gameObject.SetActive(true);
                break;
            case MenuMode.OnStop:
                NextLevelButton.gameObject.SetActive(true);
                ReplayButton.gameObject.SetActive(false);
                break;
        }
    }
}
